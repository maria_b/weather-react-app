'use strict'
/**
 * The user will input a city and after pressing the "Get Weather" button,
 * the open weather api should return a JSON string in response with the weather 
 * of the city with an icon representing the weather from the same API. In the case 
 * that we have an error, the message "city not found " will be displayed on the page.
 * @author Maria Barba
 * @version 12-2020
 */
/**
 * This function will setup the WeatherInCity in the form using react
 */
function setup() {
    ReactDOM.render(<WeatherInCity />, document.querySelector("#form"));
}
let APIKEY = {
    apiKey: "8c05dffe57c857d053035b8a7c837055"
}
class WeatherInCity extends React.Component {
    constructor(props) {
        //create a constructor to initialize state and bind methods
        super(props);
        //call super constructor
        this.state = {
            city: "hanoi",
            //sets the initial city to be Hanoi
            showError: false,
            //false if we don't need to display an error true if
            //we need to display an error
            showResult: false,
            //false if we don't need to display the result (in case of error)
            //true is we need to display the result (no error)
            errorMessage: "",
            //stores the error message
            weatherIcon: "",
            //stores the weather icon
            backColour: "",
            //changes background colour 
            iconURL: "",
            //has the iconURL
            description: "",
            temperature: ""
        }
        this.apiURL = "https://api.openweathermap.org/data/2.5/weather?";
        this.imageURL = "https://openweathermap.org/img/wn/"
        this.handleChange = this.change.bind(this);
        //attach an event handler when the input in the form is changed
        this.handleSubmit = this.submit.bind(this);
        //attach an event handler when the user submits the result

    }
    change(e) {
        if (e.target.name == "city") {
            this.setState({ city: e.target.value })
            //if the element that is changed is the city name,we set it to the
            //new city name
        }
    }
    submit(e) {
        e.preventDefault();
        //prevent form data from submitting
        this.getWeather();
        //call the getWeather func
    }
    /**
     * Read the city from the form,construct the URL
     * and retrieve the data
     */
    getWeather() {
        const data = {
            q: this.state.city,
            units: "metric",
            appid: APIKEY.apiKey
        }
        const myParams = new URLSearchParams(data);
        const url = this.apiURL + myParams;
        //will construct a URL with the city search parameters that the user 
        //input in the form and the unique APIKEY
        //set the appid
        fetch(url)
            .then(response => {
                if ((response.ok)) {
                    return response.json();
                    //if there is an error, throw the error
                } else {
                    throw new Error(`This error occured : ${response.status}`);
                }
            }).then(json => this.weatherRender(json))
            .catch(e => { this.displayError(e); });
        //when there is an error, pass the error and url to displayError
        //otherwise when there is no error, pass the data to weatherReander()
    }
    displayError(e) {
        
        if (e.message.includes("404")) {
            //if we get a 404 status code for our error; we show the output    
            this.setState({ showError: true, showResult: false, errorMessage: "City not found !", backColour: "red" });
        } else {
            this.setState({ showError: true, showResult: false, errorMessage: e.message, backColour: "yellow" });
            //if we get any other kind of error,we display the appropriate message
        }
    }
    /**
     * Provided with the json data from the API,show the 
     * weather description and the temperature 
     * @param {JSON} jsonData
     */
    weatherRender(jsonData) {
        
        this.setState({ description: jsonData.weather[0].description });
        this.setState({ temperature: jsonData.main.temp });
        this.setState({ iconURL: this.imageURL + jsonData.weather[0].icon + ".png", backColour: "LightBlue" });
        this.setState({ showResult: true });
        this.setState({ showError: false });
        //set the weather description,temperature (transform farenheit into celsius)
        // get the icon code and construct the icon url
    }

    /**
     * The render method will create a styleResult and a styleError to be applied as style
     * to the resultOutput or the errorOutput divs.The render method will also set up the UI 
     * that should be displayed as the page renders and the state changes.
     */
    render() {
        let styleResult = {
            backgroundColor: this.state.backColour,
            display: this.state.showResult ? "block" : "none",
        }
        //will store the background color, and track if the result should be displayed
        let styleError = {
            backgroundColor: this.state.backColour,
            display: this.state.showError ? "block" : "none",
        }
        //will store the background color, and track if the error should be displayed
        let styleWeatherIcon = {
            height: "100px",
            width: "100px"
        }
        //will store the style for the image 
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <fieldset id="latlong">
                        <legend>Write the city of your choice to find out the weather </legend>
                        <label htmlFor="lat">City</label>
                        <input type="text" id="city" name="city" required value={this.state.city}
                            onChange={this.handleChange} />
                        <br />
                    </fieldset>
                    <input type="submit" onSubmit={() => this.submit()} id="submit" value="Get Weather"></input>
                </form>
                <div id='resultOutput' style={styleResult} >
                    <p >Weather: {this.state.description} Temperature: {this.state.temperature} degrees celsius </p>
                    <br></br>
                    <div><img style={styleWeatherIcon} rel="Weather Icon" src={this.state.iconURL}></img></div>

                </div>
                <div id='errorOutput' style={styleError}>
                    <p >Error: {this.state.errorMessage} </p>
                </div>
            </div>
        );
    }

}
setup()
//will call the setup function to set up component